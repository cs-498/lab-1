import numpy
import sys
import matplotlib.pyplot as plt

class Node(object):

    def __init__(self,coord = (0,0), parent = 0, end = (0,0), length = 0):
        self.coord = coord
        self.parent = parent
        self.x = coord[0]
        self.y = coord[1]
        self.end = end
        self.length = length

    def getcoord(self):
        return self.coord

    def getparent(self):
        return self.parent

    def getdist(self):
        dist = abs(self.x - self.end[0])+abs(self.y - self.end[1])
        return dist

def getNeighbors(objectMap, point):
    if(point.parent == 0):
        possibleNeighbors = [
            (point.x + 1, point.y),
            (point.x - 1, point.y),
            (point.x, point.y + 1),
            (point.x, point.y - 1)
        ]
    elif(point.x - point.parent.x == 0):
        possibleNeighbors = [
            (point.x, point.y + 1),
            (point.x, point.y - 1),
            (point.x + 1, point.y),
            (point.x - 1, point.y)
        ]
    else:
        possibleNeighbors = [
            (point.x + 1, point.y),
            (point.x - 1, point.y),
            (point.x, point.y + 1),
            (point.x, point.y - 1)
        ]
    neighbors = []
    rows = objectMap.shape[0]
    cols = objectMap.shape[1]
    for r, c in possibleNeighbors:
        #check if the move is valid
        if r >= 0 and r < rows and c >= 0 and c < cols and objectMap[r, c] == 0:
            neighbors.append((r,c))
    return neighbors

def AStar(maze, end):
        
    dim = maze.shape
    visited = [[0 for j in range(dim[1])] for i in range(dim[0])]
   
    start = Node((50,0), 0, end)
    num_states_explored = 0
    path = []
    frontier = []

    frontier.append(start)
    visited[start.x][start.y] = 1

    temp = 0

    while len(frontier) > 0:
        dist = dim[0]*dim[1]
        for j in range(len(frontier)-1,-1,-1):
            if ((frontier[j].getdist() + frontier[j].length) < dist):
                dist = frontier[j].getdist() + frontier[j].length
                index = j

        point = frontier.pop(index)
        neighbors = getNeighbors(maze, point)
        num_states_explored = num_states_explored + 1

        for i in range(0, len(neighbors)):
            if (visited[neighbors[i][0]][neighbors[i][1]] == 0):
                next_point = Node(neighbors[i], point, end, point.length+1)               
                frontier.insert(0, next_point)
                visited[next_point.x][next_point.y] = 1

                if (neighbors[i][0] == end[0]) and (neighbors[i][1] == end[1]):
                    frontier = []
                    break

    path.append(next_point.getcoord())
    while next_point.getparent() != 0:
        next_point = next_point.getparent()
        path.append(next_point.getcoord())

    path.reverse()
    #print(path)

    return path


