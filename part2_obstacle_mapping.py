import picar_4wd as fc
import sys
import tty
import termios
import asyncio
import time
import random
import numpy
import matplotlib.pyplot as plt
import math
import threading
import scipy
from scipy import ndimage
from astar import AStar
from detect_picamera import main

objectMap = numpy.zeros((100,100))
imageCount = 0
halt = [False]
killed = [False]

def ScanSurroundings():
    global objectMap
    objectMap = numpy.zeros((100,100))
    for angle in range(-90, 90, 2):
        dist = fc.get_distance_at(angle)
        x = math.sin(math.radians(angle))*dist
        y = math.cos(math.radians(angle))*dist        
        #print(dist, " at ", angle, " degrees [", x, ", ",y,"]")
        if(dist > 5):
            if(x >= -50 and x <= 50 and y <= 100 and y >= 0):
                objectMap[math.floor(x+50),math.floor(y)] = 1

def AddObjectToUnknownAreas(objectMap):
    carSize = numpy.ones((12,12)) 
    objectMap = 2*scipy.ndimage.morphology.binary_dilation(objectMap, carSize).astype(objectMap.dtype) - objectMap
    [rows, columns] = objectMap.shape
    carPosition = (50,0)
    for i in range(rows):
        for j in range(columns):
            if(objectMap[i][j] == 1 or (i == carPosition[0] and j == carPosition[1])):
                continue
            dist = math.sqrt((i-carPosition[0])**2 + (j-carPosition[1])**2)
            angle = math.atan2((j-carPosition[1]),(i-carPosition[0]))
            foundBarrier = False
            for k in range(5,math.ceil(dist)+1):                
                x = round(k*math.cos(angle)) + carPosition[0]
                y = round(k*math.sin(angle)) + carPosition[1]
                if(x >= rows or y >= columns):
                    continue
                if(foundBarrier and objectMap[x][y] == 0):
                    objectMap[x][y] = 3;
                elif(objectMap[x][y] == 1 or objectMap[x][y] == 2):
                    foundBarrier = True
    return objectMap

def GetNextTargetPosition(targetPos,curPos):
    global objectMap
    [rows, columns] = objectMap.shape
    nextTarget = (50,min(round(numpy.linalg.norm(targetPos-curPos)), 99))
    print(nextTarget)
    if(objectMap[round(nextTarget[0])][round(nextTarget[1])] == 0):
        return nextTarget
    for dist in range(1,100):
        borderRange = numpy.linspace(-dist,dist,dist*2+1)
        borderMax = numpy.ones_like(borderRange)*dist
        borderX = []
        borderX.extend((-borderMax).tolist())
        borderX.extend(borderRange.tolist())
        borderX.extend(borderMax.tolist())
        borderX.extend((-borderRange).tolist())
        borderY = []
        borderY.extend((-borderRange).tolist())
        borderY.extend((-borderMax).tolist())
        borderY.extend(borderRange.tolist())
        borderY.extend(borderMax.tolist())
        for i in range(0,len(borderX)):
            testTarget = nextTarget + numpy.asarray([borderX[i],borderY[i]]).astype(int)
            if(testTarget[0] >= rows or testTarget[1] >= columns):
                continue
            if(objectMap[testTarget[0]][testTarget[1]] == 0):
                return testTarget
    return False

def PlotMap(name):
    global objectMap, imageCount
    #plt.ion()
    fig = plt.figure("Map")
    mapTranspose = numpy.transpose(objectMap)
    plt.imshow(mapTranspose)
    plt.gca().invert_yaxis()
    plt.gca().invert_xaxis()
    #fig.savefig(name + str(imageCount) + '.png')
    fig.savefig(name + '.png')
    imageCount += 1
    #plt.show()

def moveDist(dist_cm):
    global halt    
    fc.forward(15)
    x = 0
    while x < dist_cm:
        if(halt[0] == True):
            fc.stop()
            print("HALT CAR")
        else:
            fc.forward(15)
        time.sleep(0.1)        
        speed = fc.speed_val()
        x += speed*0.1
        #print("%scm/s"%speed, " : ", halt[0])

    print("Traveled %s cm"%x)
    fc.stop()

def rotate(direction, angle):
    if(direction == 1):
        fc.turn_right(50)
        print("Turn Right by",angle)
    else:
        fc.turn_left(50)
        print("Turn Left by",angle)
    turnTime = 1.2*angle/90
    time.sleep(turnTime)
    fc.stop()

def pathToTurnSequence(path):
    turnSequence = []
    distances = []
    lastNode = None
    dist = 0
    yDirection = True
    for node in path:
        if(lastNode == None):
            lastNode = node
            continue
        if(yDirection):
            if(node[0] != lastNode[0]):
                distances.append(dist)
                dist = 0
                yDirection = False
                if(node[0] > lastNode[0]):
                    turnSequence.append(-1)
                else:
                    turnSequence.append(1)
        else:
            if(node[1] != lastNode[1]):
                distances.append(dist)
                dist = 0
                yDirection = True
                if(node[1] > lastNode[1]):
                    turnSequence.append(-1)
                else:
                    turnSequence.append(1)
        dist += 1
        lastNode = node

    distances.append(dist)
    return distances, turnSequence
                
def ObstacleAvoidance():
    global objectMap
    power = 0
    angle = 0
    maxPower = 0
    fc.start_speed_thread()

    targetPos= numpy.asarray([0,200])
    curPos = numpy.asarray([0,0])
    angle = 90
    
    while True:
        if(numpy.array_equal(curPos, targetPos)):
            break
        angleToTarget = numpy.rad2deg(math.atan2(targetPos[1] - curPos[1], targetPos[0] - curPos[0]))
        deltaAngle = angleToTarget - angle
        rotate(numpy.sign(deltaAngle), abs(deltaAngle))
        angle = angleToTarget
        print("New angle = ", angle)
        ScanSurroundings()          
        PlotMap("Detection")
        print("Update Map and get find best target position")
        objectMap = AddObjectToUnknownAreas(objectMap)
        nextTarget = GetNextTargetPosition(targetPos,curPos)          
        print("Entering AStar",nextTarget)  
        path = AStar(objectMap, nextTarget)
        print("Exit AStar")
        for i, j in path:
            objectMap[i, j]= 4
        distances, turns = pathToTurnSequence(path)
        print(distances, turns)
        PlotMap("Path")
        for index in range(0,len(distances)):
            moveDist(distances[index])
            curPos = (curPos[0] + round(distances[index] * math.cos(numpy.deg2rad(angle))), curPos[1] + round(distances[index] * math.sin(numpy.deg2rad(angle))))
            if(curPos == tuple(targetPos)):
                print("Success!")
                return
            if(index < len(turns)):
                rotate(turns[index], 90)
                angle = angle + turns[index] * 90
            print("Position: ",curPos, " cm")
            print("Angle: ", angle, " deg")
           
def ImageDetection():
    global halt
    global killed
    main(halt, killed)
         
        
if __name__ == '__main__':
    camThread = threading.Thread(target=ImageDetection)    
    try:        
        camThread.start()
        ObstacleAvoidance()
        killed[0] = True
        print("Got Here")
    finally:
        fc.stop()





